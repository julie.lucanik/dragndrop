import React from 'react'
import { number, func } from 'prop-types'
import Icon from '@material/react-material-icon'

const Pagination = ({
  currentPage,
  numPages,
  handleChangePage,
  handleChoosePage,
}) => {
  return (
    <div className="pagination-pdf-container">
      <Icon icon="first_page" onClick={() => handleChangePage('first')} />
      <Icon icon="chevron_left" onClick={() => handleChangePage('previous')} />
      <span className="pagination-pages">
        <input
          id="inputPagination"
          className="input-pagination"
          type="text"
          value={currentPage + 1 || ''}
          onChange={e => handleChoosePage(e.target.value)}
        />
        {`/ ${numPages || 1}`}
      </span>
      <Icon icon="chevron_right" onClick={() => handleChangePage('next')} />
      <Icon icon="last_page" onClick={() => handleChangePage('last')} />
    </div>
  )
}

Pagination.defaultProps = {
  numPages: 1,
}

Pagination.propTypes = {
  currentPage: number.isRequired,
  numPages: number,
  handleChangePage: func.isRequired,
  handleChoosePage: func.isRequired,
}

export default Pagination
