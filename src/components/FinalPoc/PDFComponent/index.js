/* eslint-disable react/forbid-prop-types */
import React from 'react'
import { func, string, number, any } from 'prop-types'
import { Document, Page } from 'react-pdf/dist/entry.webpack'
import 'react-pdf/dist/Page/AnnotationLayer.css'

import './style.css'

const options = {
  cMapUrl: 'cmaps/',
  cMapPacked: true,
}

const PDF = ({ setNumPages, file, currentPage, fileType }) => {
  const portraitWidth = window.innerHeight > 830 ? 550 : 450
  const onDocumentLoadSuccess = document => {
    setNumPages(document.numPages)
  }

  return (
    <div className="poc-pdf">
      <div className="poc-pdf-container">
        <div className="poc-pdf-container-document">
          <Document
            file={file}
            onLoadSuccess={onDocumentLoadSuccess}
            options={options}
          >
            <>
              <Page
                pageNumber={currentPage + 1}
                width={fileType === 'portrait' ? portraitWidth : 700}
              />
            </>
          </Document>
        </div>
      </div>
    </div>
  )
}

PDF.propTypes = {
  setNumPages: func.isRequired,
  file: any.isRequired,
  currentPage: number.isRequired,
  fileType: string.isRequired,
}

export default PDF
