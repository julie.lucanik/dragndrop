/* eslint-disable max-len */
import React, { useState } from 'react'
import update from 'immutability-helper'

import PDFComponent from './PDFComponent'
import DropZone from './DropZone'
import User from './DropZone/User'
import PortraitPDF from './PDFComponent/sample.pdf'
import LandscapePDF from './PDFComponent/landscape.pdf'
import PDFPagination from './PDFComponent/PDFPagination'
import { mockUsers } from './mockUsers'

function FinalPoc() {
  const [users, setUsers] = useState(mockUsers)
  const [pdfFile, setPdfFile] = useState({
    type: 'portrait',
    file: PortraitPDF,
  })
  const [numPages, setNumPages] = useState(null)
  const [currentPage, setCurrentPage] = useState(0)

  const removeUser = id => {
    setUsers(
      update(users, {
        [id]: {
          $merge: {
            left: 0,
            top: 0,
            isInDropZone: false,
            page: {
              num: null,
              orientation: null,
              width: null,
              height: null,
            },
          },
        },
      }),
    )
  }

  const handleChangePage = (direction = 'next') => {
    let value
    switch (direction) {
      case 'next':
        value = currentPage + 1
        break
      case 'previous':
        value = currentPage - 1
        break
      case 'last':
        value = numPages - 1
        break
      case 'first':
      default:
        value = 0
    }

    if (value > numPages - 1 || value < 0) return
    setCurrentPage(value)
  }

  const handleChoosePage = page => {
    const onlyNum = /^[0-9]+$/
    const isValid = page.match(onlyNum)

    if (!page) {
      setCurrentPage(0)
      return
    }
    if (!isValid || page > numPages || page < 0) return

    setCurrentPage(parseInt(page, 10) - 1)
  }
  const handleChangePDF = () => {
    const value =
      pdfFile.type === 'portrait'
        ? { type: 'landscape', file: LandscapePDF }
        : { type: 'portrait', file: PortraitPDF }

    setPdfFile(value)
    setUsers(mockUsers)
    setCurrentPage(0)
  }

  // To convert position of users to its A4 position
  // const convertPositionToA4 = user => {
  //   const {
  //     top,
  //     left,
  //     page: { width, height, orientation },
  //   } = user
  //   const A4 =
  //     orientation === 'portrait'
  //       ? { heightA4: 842, widthA4: 595 }
  //       : { widthA4: 842, heightA4: 595 }

  //   const resizedTop = top > 0 ? Math.floor((top * A4.heightA4) / height) : 0
  //   const resizedLeft = left > 0 ? Math.floor((left * A4.widthA4) / width) : 0

  //   return { ...user, top: resizedTop, left: resizedLeft }
  // }

  // const setOrdinateAxisOriginToBottom = (user) => {
  //   const {
  //     top,
  //     userCardHeight,
  //     page: {height },
  //   } = user;
  //   return Math.max(0,height-userCardHeight-top)
  // }

  return (
    <>
      <div className="poc-container">
        <div>
          <PDFPagination
            currentPage={currentPage}
            handleChangePage={handleChangePage}
            handleChoosePage={handleChoosePage}
            numPages={numPages}
          />
          <DropZone
            removeUser={removeUser}
            users={users}
            setUsers={setUsers}
            currentPage={currentPage}
          >
            <PDFComponent
              file={pdfFile.file}
              fileType={pdfFile.type}
              currentPage={currentPage}
              setNumPages={setNumPages}
            />
          </DropZone>
        </div>
        <div className="layout-column-flex">
          <button
            className="button-switch-pdf"
            type="button"
            onClick={handleChangePDF}
          >
            Switch PDF
          </button>
          <div className="drag-container">
            <p className="subtitle">Users</p>
            <div className="user-container">
              {numPages &&
                Object.keys(users).map(key => {
                  const { name, isInDropZone } = users[key]
                  if (isInDropZone) return null
                  return (
                    <User
                      key={key}
                      id={key}
                      removeUser={removeUser}
                      // eslint-disable-next-line react/jsx-props-no-spreading
                      {...users[key]}
                    >
                      {name}
                    </User>
                  )
                })}
            </div>
          </div>
          <div className="info-container">
            <p className="subtitle">Info</p>
            {Object.keys(users).map(key => {
              const {
                name,
                isInDropZone,
                top,
                left,
                page: { num, orientation },
              } = users[key]
              if (isInDropZone) {
                return (
                  <p className="info-content" key={key}>
                    <strong>{name}</strong>

                    <span>{` signed on page ${num}. ( top: ${top}px - left: ${left}px - orientation: ${orientation} ).`}</span>
                  </p>
                )
              }

              return (
                <p className="info-content" key={key}>
                  <strong>{name}</strong>
                  <span>{` has not signed yet.`}</span>
                </p>
              )
            })}
          </div>
        </div>
      </div>
    </>
  )
}

export default FinalPoc
