export const mockUsers = {
  a: {
    top: 0,
    left: 0,
    name: 'Lara Croft',
    isInDropZone: false,
    userCardHeight: 36,
    page: {
      num: null,
      orientation: null,
      width: null,
      height: null,
    },
  },
  b: {
    top: 0,
    left: 0,
    name: 'Marty Mcfly',
    isInDropZone: false,
    userCardHeight: 36,
    page: {
      num: null,
      orientation: null,
      width: null,
      height: null,
    },
  },
}
