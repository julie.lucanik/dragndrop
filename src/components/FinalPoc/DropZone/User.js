import React from 'react'
import { shape, string, number, bool, node, func } from 'prop-types'
import { useDrag } from 'react-dnd'
import { ItemTypes } from './ItemTypes'

const User = ({
  id,
  left,
  top,
  children,
  isInDropZone,
  removeUser,
  page: { num, orientation },
}) => {
  const [{ isDragging }, drag] = useDrag({
    item: {
      id,
      type: ItemTypes.USER,
      left,
      top,
      isInDropZone,
      page: num,
      pageOrientation: orientation,
    },
    collect: monitor => ({
      isDragging: !!monitor.isDragging(),
    }),
    end: (dropResult, monitor) => {
      const { id } = monitor.getItem()
      const didDrop = monitor.didDrop()
      if (!didDrop) {
        removeUser(id)
      }
    },
  })

  // To hide the element initial position during its dragging
  if (isDragging) {
    return <div ref={drag} />
  }

  return (
    <div
      ref={drag}
      className="user"
      style={isInDropZone ? { position: 'absolute', left, top } : {}}
    >
      {children}
    </div>
  )
}

User.defaultProps = {
  page: {
    num: null,
    orientation: null,
  },
}

User.propTypes = {
  id: string.isRequired,
  top: number.isRequired,
  left: number.isRequired,
  isInDropZone: bool.isRequired,
  page: shape({
    num: number,
    orientation: string,
  }),
  children: node.isRequired,
  removeUser: func.isRequired,
}

export default User
