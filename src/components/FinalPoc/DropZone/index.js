import React from 'react'
import { shape, string, number, bool, func, node } from 'prop-types'
import { useDrop } from 'react-dnd'
import update from 'immutability-helper'
import { ItemTypes } from './ItemTypes'
import User from './User'

const DropZone = ({ users, setUsers, removeUser, children, currentPage }) => {
  const moveUser = (id, left, top, page) => {
    setUsers(
      update(users, {
        [id]: {
          $merge: {
            left,
            top,
            isInDropZone: true,
            page: {
              num: currentPage + 1,
              orientation: page.orientation,
              width: page.width,
              height: page.height,
            },
          },
        },
      }),
    )
  }

  const [, drop] = useDrop({
    accept: ItemTypes.USER,
    drop(item, monitor) {
      const parent = document.getElementById('pocDropZone')

      const delta = monitor.getDifferenceFromInitialOffset()

      const left = item.isInDropZone
        ? Math.round(item.left + delta.x)
        : Math.round(
            Math.abs(monitor.getSourceClientOffset().x) - parent.offsetLeft,
          )
      const top = item.isInDropZone
        ? Math.round(item.top + delta.y)
        : Math.round(
            Math.abs(monitor.getSourceClientOffset().y) - parent.offsetTop,
          )

      const orientation =
        parent.clientHeight > parent.clientWidth ? 'portrait' : 'landscape'
      moveUser(item.id, Math.max(0, left), Math.max(0, top), {
        orientation,
        width: parent.clientWidth,
        height: parent.clientHeight,
      })
      return undefined
    },
  })

  return (
    <div ref={drop} id="pocDropZone" style={{ position: 'relative' }}>
      {children}
      {Object.keys(users).map(key => {
        const {
          name,
          isInDropZone,
          page: { num },
        } = users[key]
        if (!isInDropZone || (num && num - 1 !== currentPage)) return null

        return (
          // eslint-disable-next-line react/jsx-props-no-spreading
          <User removeUser={removeUser} key={key} id={key} {...users[key]}>
            {name}
          </User>
        )
      })}
    </div>
  )
}

DropZone.propTypes = {
  users: shape({
    id: shape({
      top: number,
      left: number,
      name: string,
      isInDropZone: bool,
      page: shape({
        num: number,
        orientation: string,
        width: number,
        height: number,
      }),
    }),
  }).isRequired,
  setUsers: func.isRequired,
  removeUser: func.isRequired,
  children: node.isRequired,
  currentPage: number.isRequired,
}

export default DropZone
