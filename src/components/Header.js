import React from 'react'
import { NavLink } from 'react-router-dom'

const Header = () => {
  return (
    <nav className="header-nav">
      <div className="header-title-container">
        <img src="/logo192.png" alt="logo" className="logo" />
        <div className="header-content">
          <h1 className="header-title">Position Signature on PDF</h1>
          <p className="header-subtitle">POC</p>
        </div>
      </div>
      <ul className="nav-items">
        <li className="nav-item">
          <NavLink to="/dnd" activeClassName="selected">
            Drag N Drop
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/pdf" activeClassName="selected">
            PDF
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/final" activeClassName="selected">
            Final Poc
          </NavLink>
        </li>
      </ul>
    </nav>
  )
}

export default Header
