import React, { useState } from 'react'
import update from 'immutability-helper'

import DropZone from './DropZone'
import UserDragged from './UserDragged'

const DragNDropView = () => {
  const [users, setUsers] = useState({
    a: { top: 0, left: 0, name: 'Leroy Jethro Gibbs', isInDropZone: false },
    b: { top: 0, left: 0, name: 'Cersei Lannister', isInDropZone: false },
  })

  const removeUser = id => {
    setUsers(
      update(users, {
        [id]: {
          $merge: { left: 0, top: 0, isInDropZone: false },
        },
      }),
    )
  }
  return (
    <div className="dnd-container">
      <div className="drop-zone-container">
        <DropZone removeUser={removeUser} users={users} setUsers={setUsers} />
      </div>
      <div className="drag-container">
        <p className="drag-title">Users</p>
        <div className="user-container">
          {Object.keys(users).map(key => {
            const { name, isInDropZone } = users[key]
            if (isInDropZone) return null
            return (
              <UserDragged
                key={key}
                id={key}
                removeUser={removeUser}
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...users[key]}
              >
                {name}
              </UserDragged>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default DragNDropView
