import React from 'react'
import { string, number, bool, node, func } from 'prop-types'
import { useDrag } from 'react-dnd'

const ItemTypes = {
  USER: 'user',
}

const UserDragged = ({ id, left, top, children, isInDropZone, removeUser }) => {
  const [{ isDragging }, drag] = useDrag({
    item: { id, left, top, type: ItemTypes.USER, isInDropZone },
    collect: monitor => ({
      isDragging: !!monitor.isDragging(),
    }),
    end: (dropResult, monitor) => {
      const { id } = monitor.getItem()
      const didDrop = monitor.didDrop()
      if (!didDrop) {
        removeUser(id)
      }
    },
  })
  // To hide the element initial position during its dragging
  if (isDragging) {
    return <div ref={drag} />
  }

  return (
    <div
      ref={drag}
      className="user"
      style={isInDropZone ? { position: 'absolute', left, top } : {}}
    >
      {children}
    </div>
  )
}

UserDragged.propTypes = {
  id: string.isRequired,
  top: number.isRequired,
  left: number.isRequired,
  isInDropZone: bool.isRequired,
  children: node.isRequired,
  removeUser: func.isRequired,
}

export default UserDragged
