import React from 'react'
import { shape, string, number, bool, func } from 'prop-types'

import { useDrop } from 'react-dnd'
import update from 'immutability-helper'
import UserDragged from './UserDragged'

const ItemTypes = {
  USER: 'user',
}

const DropZone = ({ users, setUsers, removeUser }) => {
  const moveUser = (id, left, top) => {
    setUsers(
      update(users, {
        [id]: {
          $merge: { left, top, isInDropZone: true },
        },
      }),
    )
  }

  const [, drop] = useDrop({
    accept: ItemTypes.USER,
    drop(item, monitor) {
      const parent = document.getElementById('dropzone')
      const delta = monitor.getDifferenceFromInitialOffset()

      const left = item.isInDropZone
        ? Math.round(item.left + delta.x)
        : Math.round(
            Math.abs(monitor.getSourceClientOffset().x) - parent.offsetLeft,
          )
      const top = item.isInDropZone
        ? Math.round(item.top + delta.y)
        : Math.round(
            Math.abs(monitor.getSourceClientOffset().y) - parent.offsetTop,
          )
      moveUser(item.id, Math.max(0, left), Math.max(0, top))
      return undefined
    },
  })

  return (
    <div ref={drop} id="dropzone" className="drop-zone">
      {Object.keys(users).map(key => {
        const { name, isInDropZone } = users[key]
        if (!isInDropZone) return null

        return (
          <UserDragged
            removeUser={removeUser}
            key={key}
            id={key}
            // eslint-disable-next-line react/jsx-props-no-spreading
            {...users[key]}
          >
            {name}
          </UserDragged>
        )
      })}
    </div>
  )
}

DropZone.propTypes = {
  users: shape({
    id: shape({
      top: number,
      left: number,
      name: string,
      isInDropZone: bool,
    }),
  }).isRequired,
  setUsers: func.isRequired,
  removeUser: func.isRequired,
}

export default DropZone
