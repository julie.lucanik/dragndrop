import React, { useState } from 'react'
import { Document, Page } from 'react-pdf/dist/entry.webpack'
import 'react-pdf/dist/Page/AnnotationLayer.css'

import './style.css'
import SamplePDF from './sample.pdf'
import PaginationPDF from './PaginationPDF'

const options = {
  cMapUrl: 'cmaps/',
  cMapPacked: true,
}

const PDFView = () => {
  const [file] = useState(SamplePDF)
  const [numPages, setNumPages] = useState(null)
  const [currentPage, setCurrentPage] = useState(0)

  const onDocumentLoadSuccess = document => {
    setNumPages(document.numPages)
  }

  const handleChangePage = (direction = 'next') => {
    const value = direction === 'next' ? currentPage + 1 : currentPage - 1

    if (value > numPages - 1 || value < 0) return
    setCurrentPage(value)
  }

  return (
    <div className="pdf-example">
      <div className="pdf-example-container">
        <PaginationPDF
          currentPage={currentPage}
          handleChangePage={handleChangePage}
          numPages={numPages}
        />
        <div className="pdf-example-container-document">
          <Document
            file={file}
            onLoadSuccess={onDocumentLoadSuccess}
            options={options}
          >
            <>
              <Page
                key={`page_${currentPage + 1}`}
                pageNumber={currentPage + 1}
              />
            </>
          </Document>
        </div>
      </div>
    </div>
  )
}

export default PDFView
