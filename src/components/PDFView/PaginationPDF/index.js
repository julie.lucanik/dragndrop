import React from 'react'
import { number, func } from 'prop-types'
import Icon from '@material/react-material-icon'

const PaginationPDF = ({ currentPage, numPages, handleChangePage }) => {
  return (
    <div className="pagination-pdf-container">
      <Icon icon="chevron_left" onClick={() => handleChangePage('previous')} />
      <span className="pagination-pages">
        {`${currentPage + 1} / ${numPages || 1}`}
      </span>
      <Icon icon="chevron_right" onClick={() => handleChangePage('next')} />
    </div>
  )
}

PaginationPDF.defaultProps = {
  numPages: 1,
}

PaginationPDF.propTypes = {
  currentPage: number.isRequired,
  numPages: number,
  handleChangePage: func.isRequired,
}

export default PaginationPDF
