import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'

import Header from './components/Header'
import DragNDropView from './components/DragNDropView'
import PDFView from './components/PDFView'
import FinalPoc from './components/FinalPoc'

import '@material/react-material-icon/dist/material-icon.css'

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <Router>
        <div className="app-container">
          <Header />
          <Switch>
            <Route exact path="/pdf">
              <PDFView />
            </Route>
            <Route exact path="/dnd">
              <DragNDropView />
            </Route>
            <Route exact path="/final">
              <FinalPoc />
            </Route>
            <Redirect to="/final" />
          </Switch>
        </div>
      </Router>
    </DndProvider>
  )
}

export default App
